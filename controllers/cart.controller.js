exports.getCart = (req, res, Cart) => {
    Cart.find({}, (err, found)=>{
        if(err) {
            return res.status(500).json({error: err.toString()});
        }
        res.json({cart: found})
    });
};

exports.createCart= (req, res, Cart) => {
    let cart = req.body;
    Cart.create(cart, (err, inserted)=>{
        console.log(inserted);
        if(err){
            return res.status(500).json({error: err.toString()});
        }
        res.json({cart: inserted});
    });
};

exports.deleteCart = (req, res, Cart) => {
    let id = req.params._id;
    Cart.findByIdAndRemove(id, (err, deleted) => {
        if (err) {
            return res.status(500).json({error: err.toString()});
        }
        res.json({cart: deleted});
    });
};

exports.updateCart = (req, res, Cart) => {
    let id = req.params._id;
    let cart = req.body;
    Cart.findByIdAndUpdate(id, cart, (err, updated) => {
        if(err){
            return res.status(500).json({erros: err.toString()});
        }
        res.json({cart: updated});
    });
};