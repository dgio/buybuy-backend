exports.getOrders = (req, res, Order) => {
    Order.find({}, (err, found)=>{
        if(err) {
            return res.status(500).json({error: err.toString()});
        }
        res.json({orders: found})
    });
};

exports.getOrderById = (req, res, Order) => {
    let id = req.params._id;
    Order.findOne({_id: id}, (err, found)=>{
        if(err) {
            return res.status(500).json({error: err.toString()});
        }
        res.json({orders: found})
    });
};

exports.createOrder= (req, res, Order) => {
    let order = req.body;
    Order.create(order, (err, inserted)=>{
        console.log(inserted);
        if(err){
            return res.status(500).json({error: err.toString()});
        }
        res.json({orders: inserted});
    });
};

exports.deleteOrder = (req, res, Order) => {
    let id = req.params._id;
    Order.findByIdAndRemove(id, (err, deleted) => {
        if (err) {
            return res.status(500).json({error: err.toString()});
        }
        res.json({orders: deleted});
    });
};

exports.updateOrder = (req, res, Order) => {
    let id = req.params._id;
    let order = req.body;
    Order.findByIdAndUpdate(id, order, (err, updated) => {
        if(err){
            return res.status(500).json({erros: err.toString()});
        }
        res.json({orders: updated});
    });
};