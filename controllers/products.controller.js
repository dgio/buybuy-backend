exports.getProducts = (req, res, Product) => {
    Product.find({}, (err, found)=>{
        if(err) {
            return res.status(500).json({error: err.toString()});
        }
        res.json({products: found})
    });
};
exports.getProductById = (req, res, Product) => {
    let id = req.params._id;
    Product.findOne({_id: id}, (err, found)=>{
        if(err) {
            return res.status(500).json({error: err.toString()});
        }
        res.json({products: found})
    });
};
exports.getProductByCompany = (req, res, Product) => {
    let company = req.params.company;
    Product.find({company: company}, (err, found)=>{
        if(err) {
            return res.status(500).json({error: err.toString()});
        }
        res.json({products: found})
    });
};

exports.createProduct= (req, res, Product) => {
    let product = req.body;
    Product.create(product, (err, inserted)=>{
        console.log(inserted);
        if(err){
            return res.status(500).json({error: err.toString()});
        }
        res.json({products: inserted});
    });
};

exports.deleteProduct = (req, res, Product) => {
    let id = req.params._id;
    Product.findByIdAndRemove(id, (err, deleted) => {
        if (err) {
            return res.status(500).json({error: err.toString()});
        }
        res.json({products: deleted});
    });
};

exports.updateProduct = (req, res, Product) => {
    let id = req.params._id;
    let product = req.body;
    Product.findByIdAndUpdate(id, product, (err, updated) => {
        if(err){
            return res.status(500).json({erros: err.toString()});
        }
        res.json({products: updated});
    });
};