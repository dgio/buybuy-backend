let mongoose = require('mongoose');

let RegisterSchema = {
    email: {type: String, required: true},
    name: {type: String, required: true},
    picture: {type: String, required: false},
    authType: {type: Number, required: true},
    user: {
        name: {type: String, required: false},
        password: {type: String, required: false},
        id: {type: String, required: false}
    },
    description: {type: String, required: false},
    rfc: {type: String, required: false},
    address: {
        street: {type: String, required: false},
        suburb: {type: String, required: false},
        zipCode: {type: String, required: false},
        number: {type: String, required: false},
        city: {type: String, required: false},
        state: {type: String, required: false},
        latitude:{type: String, required: false},
        longitude: {type: String, required: false}
    },
    phone: {type: String, required: false},
    category:{type:String,required:false},
    usertype:{type:Number,required:true}
};

module.exports = mongoose.Schema(RegisterSchema);
