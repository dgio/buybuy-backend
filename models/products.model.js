let mongoose = require('mongoose');

let ProductSchema = {
    category :{type: String, required:true},
    name :{type: String, required: true},
    price :{type: Number, required: false},
    description :{type: String, required: true},
    company:{type:String,required:true}
};

module.exports = mongoose.Schema(ProductSchema);
