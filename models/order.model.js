let mongoose = require('mongoose');

let OrderSchema = {
    number: {type: String, required: true},
    status: {type: String, required: true},
    total: {type: Number, required: true},
    customer: {type: String, required: true},
    details: [{
        productName: {type: String, required: true},
        quantity: {type: Number, required: true},
        price: {type: Number, required: true}
    }]
};

module.exports = mongoose.Schema(OrderSchema);