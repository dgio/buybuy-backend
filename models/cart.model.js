let mongoose = require('mongoose');

let CartSchema = [{
    productName: {type: String, required: true},
    quantity: {type: Number, required: true},
    price: {type: Number, required: true} 
}];

module.exports = mongoose.Schema(CartSchema);