let mongoose = require('mongoose');
let undersore = require('underscore');

module.exports = (wagner)=>{
    mongoose.Promise = global.Promise;
    mongoose.connect('mongodb://localhost:27017/final-project');

    wagner.factory('db', () => mongoose);

    //User
    let User = mongoose.model(
        'User',
        require('./register.model'),
        'users'
    );

    let modelUser = { User: User };
    undersore.map(modelUser, (val, key) => {
        wagner.factory(key, () => val);
    });

    //Products
    let Product = mongoose.model(
        'Product',
        require('./products.model'),
        'products'
    );

    let modelProduct = { Product: Product };
    undersore.map(modelProduct, (val, key) => {
        wagner.factory(key, () => val);
    });

    //Orders
    let Order = mongoose.model(
        'Order',
        require('./order.model'),
        'orders'
    );

    let modelOrder = { Order: Order };
    undersore.map(modelOrder, (val, key) => {
        wagner.factory(key, () => val);
    });

    //Cart
        let Cart = mongoose.model(
        'Cart',
        require('./cart.model'),
        'cart'
    );

    let modelCart = { Cart: Cart };
    undersore.map(modelCart, (val, key) => {
        wagner.factory(key, () => val);
    });
};