let router = require('express').Router();

module.exports = (wagner) => {
    router.get('/', wagner.invoke( function(User) {
        return function(req, res){
            return require('../controllers/register.controller').getUsers(req, res, User);
        }
    }));
    router.get('/:_id', wagner.invoke( function(User) {
        return function(req, res){
            return require('../controllers/register.controller').getUserById(req, res, User);
        }
    }));
    router.get('/username/:username', wagner.invoke( function(User) {
        return function(req, res){
            return require('../controllers/register.controller').getUserByUsername(req, res, User);
        }
    }));
    router.get('/company/:category', wagner.invoke( function(User) {
        return function(req, res){
            return require('../controllers/register.controller').getUserByCategory(req, res, User);
        }
    }));

    router.get('/fb/:_id', wagner.invoke( function(User) {
        return function(req, res){
            return require('../controllers/register.controller').getUserIdFB(req, res, User);
        }
    }));

    router.post('/', wagner.invoke( function(User){
        return function (req, res){
            return require('../controllers/register.controller').createUser(req, res, User);
        }
    }));

    router.delete('/:_id', wagner.invoke(function(User){
        return function(req, res){
            return require('../controllers/register.controller').deleteUser(req, res, User);
        }
    }));

    router.put('/:_id', wagner.invoke(function(User){
        return function(req, res){
            return require('../controllers/register.controller').updateUser(req, res, User);
        }
    }));

    return router;
};
