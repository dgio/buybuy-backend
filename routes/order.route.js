let router = require('express').Router();

module.exports = (wagner) => {
    router.get('/', wagner.invoke( function(Order) {
        return function(req, res){
            return require('../controllers/order.controller').getOrders(req, res, Order);
        }
    }));
    router.get('/:_id', wagner.invoke( function(Order) {
        return function(req, res){
            return require('../controllers/order.controller').getOrderById(req, res, Order);
        }
    }));

    router.post('/', wagner.invoke( function(Order){
        return function (req, res){
            return require('../controllers/order.controller').createOrder(req, res, Order);
        }
    }));

    router.delete('/:_id', wagner.invoke(function(Order){
        return function(req, res){
            return require('../controllers/order.controller').deleteOrder(req, res, Order);
        }
    }));

    router.put('/:_id', wagner.invoke(function(Order){
        return function(req, res){
            return require('../controllers/order.controller').updateOrder(req, res, Order);
        }
    }));

    return router;
};