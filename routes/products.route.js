let router = require('express').Router();

module.exports = (wagner) => {
    router.get('/', wagner.invoke( function(Product) {
        return function(req, res){
            return require('../controllers/products.controller').getProducts(req, res, Product);
        }
    }));
    router.get('/product/:_id', wagner.invoke( function(Product) {
        return function(req, res){
            return require('../controllers/products.controller').getProductById(req, res, Product);
        }
    }));
    router.get('/:company', wagner.invoke( function(Product) {
        return function(req, res){
            return require('../controllers/products.controller').getProductByCompany(req, res, Product);
        }
    }));

    router.post('/', wagner.invoke( function(Product){
        return function (req, res){
            return require('../controllers/products.controller').createProduct(req, res, Product);
        }
    }));

    router.delete('/:_id', wagner.invoke(function(Product){
        return function(req, res){
            return require('../controllers/products.controller').deleteProduct(req, res, Product);
        }
    }));

    router.put('/:_id', wagner.invoke(function(Product){
        return function(req, res){
            return require('../controllers/products.controller').updateProduct(req, res, Product);
        }
    }));

    return router;
};