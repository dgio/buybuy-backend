let router = require('express').Router();

module.exports = (wagner) => {
    router.get('/', wagner.invoke( function(Cart) {
        return function(req, res){
            return require('../controllers/cart.controller').getCart(req, res, Cart);
        }
    }));

    router.post('/', wagner.invoke( function(Cart){
        return function (req, res){
            return require('../controllers/cart.controller').createCart(req, res, Cart);
        }
    }));

    router.delete('/:_id', wagner.invoke(function(Cart){
        return function(req, res){
            return require('../controllers/cart.controller').deleteCart(req, res, Cart);
        }
    }));

    router.put('/:_id', wagner.invoke(function(Cart){
        return function(req, res){
            return require('../controllers/cart.controller').updateCart(req, res, Cart);
        }
    }));

    return router;
};