let express = require('express');
let morgan = require('morgan');
let bodyParser = require('body-parser');
let wagner = require('wagner-core');

//Models
require('./models/models')(wagner);
let user = require('./routes/register.route')(wagner);
let product = require('./routes/products.route')(wagner);
let order = require('./routes/order.route')(wagner);
let cart = require('./routes/cart.route')(wagner);

let server = express();
server.use(morgan('dev'));
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({extended:false}));

server.use(function(req, res, next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods','GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers','X-Requested-With, content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

//Routes
server.use('/BuyBuy/users', user);
server.use('/BuyBuy/products', product);
server.use('/BuyBuy/orders', order);
server.use('/BuyBuy/cart', cart);
module.exports = server;